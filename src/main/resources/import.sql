insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (1, 'u1', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (2, 'u2', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (3, 'u3', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (4, 'u4', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (5, 'u5', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (6, 'u6', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (7, 'u7', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (8, 'u8', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (9, 'u9', 'pass', true);
insert into user_table (ID, USERNAME, PASSWORD, ENABLED) values (10, 'u10', 'pass', true);

INSERT INTO address(id, city, street, zip_code, country) VALUES (1, 'Omaha', 'Apollo  Alley, 778399136', 929053, 'France');
INSERT INTO address(id, city, street, zip_code, country) VALUES (2, 'Escondido', 'Howard Vale, 1744078882', 249023, 'Slovenia');
INSERT INTO address(id, city, street, zip_code, country) VALUES (3, 'Seattle', 'Jackson Way, 908957785', 731255, 'Guyana');
INSERT INTO address(id, city, street, zip_code, country) VALUES (4, 'Venice', 'Chandos  Rue, 1830130549', 955870, 'Brazil');
INSERT INTO address(id, city, street, zip_code, country) VALUES (5, 'Zurich', 'Blackpool  Grove, 644860275', 826724, 'Micronesia');
INSERT INTO address(id, city, street, zip_code, country) VALUES (6, 'Fayetteville', 'Buttonwood Rue, 127589200', 471219, 'Vatican City');
INSERT INTO address(id, city, street, zip_code, country) VALUES (7, 'Bellevue', 'Carolina  Grove, 887115690', 693811, 'Côte d’Ivoire');
INSERT INTO address(id, city, street, zip_code, country) VALUES (8, 'Cincinnati', 'Elia   Lane, 1103636505', 414721, 'Australia');
INSERT INTO address(id, city, street, zip_code, country) VALUES (9, 'Oklahoma City', 'Coley  Alley, 1651339093', 497286, 'Zimbabwe');
INSERT INTO address(id, city, street, zip_code, country) VALUES (10, 'Long Beach', 'Cliffords  Crossroad, 741266471', 113782, 'Malta');

insert into role values (1, 'Student');
insert into role values (2, 'Teacher');

insert into users_roles values (1, 1);
insert into users_roles values (2, 1);
insert into users_roles values (3, 2);
insert into users_roles values (3, 1);
insert into users_roles values (4, 1);
insert into users_roles values (5, 1);
insert into users_roles values (6, 2);
insert into users_roles values (7, 2);
insert into users_roles values (8, 2);
insert into users_roles values (9, 2);
insert into users_roles values (10, 2);

INSERT INTO teacher(teacher_Id, date_of_birth, first_name, last_name, phone, gender, hire_date, title, address_id) VALUES (1, '3/9/4852', 'Melanie', 'Cork', '0730355083', 'F', '6/1/4847', 'HR Coordinator', 1);
INSERT INTO teacher(teacher_Id, date_of_birth, first_name, last_name, phone, gender, hire_date, title, address_id) VALUES (2, '11/19/6588', 'Brad', 'Isaac', '0740310818', 'M', '9/30/0832', 'Software Engineer', 2);
INSERT INTO teacher(teacher_Id, date_of_birth, first_name, last_name, phone, gender, hire_date, title, address_id) VALUES (3, '4/12/4799', 'Chad', 'Cox', '0758560403', 'M', '6/21/0319', 'Software Engineer', 3);
INSERT INTO teacher(teacher_Id, date_of_birth, first_name, last_name, phone, gender, hire_date, title, address_id) VALUES (4, '10/6/5606', 'Erick', 'Archer', '0728900753', 'M', '8/26/2879', 'Baker', 4);
INSERT INTO teacher(teacher_Id, date_of_birth, first_name, last_name, phone, gender, hire_date, title, address_id) VALUES (5, '5/23/0950', 'Agnes', 'Grey', '0767233934', 'F', '11/14/6180', 'Healthcare Specialist', 5);

insert into course(id, identifier, credits, description, name, semester, year, teacher_Id) VALUES (1, 'DS', 5, 'c1', 'Data Structures', 1, 1, 1);
insert into course(id, identifier, credits, description, name, semester, year, teacher_Id) VALUES (2, 'ACSO', 5, 'c1', 'ACSO', 1, 1, 2);
insert into course(id, identifier, credits, description, name, semester, year, teacher_Id) VALUES (3, 'LOG', 4, 'c1', 'Logic', 1, 1, 3);

insert into section(id, name, percentage, course_id) VALUES (1, 'Examen', 50, 1);
insert into section(id, name, percentage, course_id) VALUES (2, 'Test', 40, 1);
insert into section(id, name, percentage, course_id) VALUES (3, 'Prezenta', 10, 1);
insert into section(id, name, percentage, course_id) VALUES (4, 'Examen1', 40, 2);
insert into section(id, name, percentage, course_id) VALUES (5, 'Examen2', 20, 2);
insert into section(id, name, percentage, course_id) VALUES (6, 'Laborator', 40, 2);

INSERT INTO student(student_id, date_of_birth, first_name, last_name, phone, gender, security_number, address_id) VALUES (1, '9/23/6253', 'Denny', 'Larsen', '0742172962', 'M', '008-164-205', 6);
INSERT INTO student(student_id, date_of_birth, first_name, last_name, phone, gender, security_number, address_id) VALUES (2, '10/8/6097', 'Julian', 'Gregory', '0766484504', 'M', '876-144-412', 7);
INSERT INTO student(student_id, date_of_birth, first_name, last_name, phone, gender, security_number, address_id) VALUES (3, '4/16/6146', 'Daron', 'Fowler','0736240369', 'M', '511-008-727', 8);
INSERT INTO student(student_id, date_of_birth, first_name, last_name, phone, gender, security_number, address_id) VALUES (4, '4/30/0384', 'Charlotte', 'Thomson', '0729794685', 'F', '085-465-183', 9);
INSERT INTO student(student_id, date_of_birth, first_name, last_name, phone, gender, security_number, address_id) VALUES (5, '10/6/2941', 'Marla', 'Garner', '0746181992', 'F', '587-622-650', 10);

insert into students_courses(student_id, course_id) VALUES (1, 1);
insert into students_courses(student_id, course_id) VALUES (1, 2);
insert into students_courses(student_id, course_id) VALUES (2, 1);
insert into students_courses(student_id, course_id) VALUES (2, 2);
insert into students_courses(student_id, course_id) VALUES (3, 1);
insert into students_courses(student_id, course_id) VALUES (3, 2);
insert into students_courses(student_id, course_id) VALUES (4, 1);
insert into students_courses(student_id, course_id) VALUES (4, 2);
insert into students_courses(student_id, course_id) VALUES (5, 1);
insert into students_courses(student_id, course_id) VALUES (5, 2);

insert into grade(id, grade_value,  section_id, student_student_id, teacher_teacher_id) values (1, 10, 1, 1, 1);
insert into grade(id, grade_value,  section_id, student_student_id, teacher_teacher_id) values (2, 5, 2, 1, 1);
insert into grade(id, grade_value,  section_id, student_student_id, teacher_teacher_id) values (3, 3, 3, 1, 1);








