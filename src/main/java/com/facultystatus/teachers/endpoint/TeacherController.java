package com.facultystatus.teachers.endpoint;

import com.facultystatus.courses.model.CourseDTO;
import com.facultystatus.teachers.model.TeacherDTO;
import com.facultystatus.teachers.model.Teacher;
import com.facultystatus.core.service.BasicValidationService;
import com.facultystatus.courses.service.CourseService;
import com.facultystatus.teachers.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nicorici Madalina (nicorici@optymyze.com)
 */
@RestController
@RequestMapping("/api/teachers")
@CrossOrigin
public class TeacherController {

    private TeacherService teacherService;
    private BasicValidationService basicValidationService;
    private CourseService courseService;

    @Autowired
    public TeacherController(TeacherService teacherService, BasicValidationService basicValidationService, CourseService courseService) {
        this.teacherService = teacherService;
        this.basicValidationService = basicValidationService;
        this.courseService = courseService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<List<TeacherDTO>> getAll() {
        List<TeacherDTO> teacherDTOS = teacherService.getAll().stream().map(t -> teacherService.convertToDto(t)).collect(Collectors.toList());
        return new ResponseEntity<>(teacherDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TeacherDTO> getById(@PathVariable Long id) {
        return teacherService.getById(id)
                .map(t -> new ResponseEntity<>(teacherService.convertToDto(t), HttpStatus.OK))
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/{id}/courses", method = RequestMethod.GET)
    public ResponseEntity<List<CourseDTO>> getCoursesById(@PathVariable Long id) {
        return teacherService.getById(id).map(t -> t.getCourses().stream()
                .map(c -> courseService.convertToDto(c)).collect(Collectors.toList()))
                .map(courseDTOS -> new ResponseEntity<>(courseDTOS, HttpStatus.OK))
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "")
    public ResponseEntity<?> addStudent(@Valid @RequestBody TeacherDTO teacherDTO, BindingResult result) {
        ResponseEntity errorsMap = basicValidationService.validate(result);
        if (errorsMap != null) {
            return errorsMap;
        }
        Teacher savedTeacher = teacherService.createOrUpdateTeacher(teacherService.convertToEntity(teacherDTO));
        return new ResponseEntity<>(savedTeacher, HttpStatus.CREATED);
    }
}
