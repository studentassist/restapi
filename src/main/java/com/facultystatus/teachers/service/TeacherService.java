package com.facultystatus.teachers.service;

import com.facultystatus.courses.service.CourseService;
import com.facultystatus.teachers.model.TeacherDTO;
import com.facultystatus.teachers.model.Teacher;
import com.facultystatus.core.repository.AddressRepository;
import com.facultystatus.teachers.repository.TeacherRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {

    private TeacherRepository teacherRepository;
    private CourseService courseService;
    private AddressRepository addressRepository;

    @Autowired
    public TeacherService(TeacherRepository teacherRepository, CourseService courseService, AddressRepository addressRepository) {
        this.teacherRepository = teacherRepository;
        this.courseService = courseService;
        this.addressRepository = addressRepository;
    }

    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }

    public Optional<Teacher> getById(Long id) {
        return teacherRepository.findById(id);
    }

    public Teacher createOrUpdateTeacher(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    public void addAddress(Teacher teacher, Long addressId) {
        addressRepository.findById(addressId).ifPresent(teacher::setAddress);

    }

    public TeacherDTO convertToDto(Teacher teacher) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(teacher, TeacherDTO.class);
    }

    public Teacher convertToEntity(TeacherDTO teacherDTO) {
        ModelMapper modelMapper = new ModelMapper();
        Teacher teacher = modelMapper.map(teacherDTO, Teacher.class);
        if (!teacherDTO.getCoursesIds().isEmpty()) {
            teacher.setCourses(courseService.getByIds(new ArrayList<>(teacherDTO.getCoursesIds())));
        }
        if (teacherDTO.getAddressId() != null) {
            addAddress(teacher, teacherDTO.getAddressId());
        }
        return teacher;
    }
}
