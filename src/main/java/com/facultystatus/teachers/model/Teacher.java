package com.facultystatus.teachers.model;

import com.facultystatus.core.model.Person;
import com.facultystatus.courses.model.Course;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

@Entity
public class Teacher extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEACHER_ID")
    private long teacherId;

    /*@OneToOne
    @PrimaryKeyJoinColumn(name = "TEACHER_ID", referencedColumnName = "id")
    private User user;*/

    private String title;

    @Column(name = "hire_date")
    @NotNull(message = "Hire date is required")
    @JsonFormat(pattern = "yyyy-dd-mm")
    private Date hireDate;

    @JsonIgnore
    @OneToMany(mappedBy = "teacher")
    private Collection<Course> courses;

    public long getTeacherId() {
        return teacherId;
    }

  /*  public User getUser() {
        return user;
    }*/

    public String getTitle() {
        return title;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public Collection<Course> getCourses() {
        return courses;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public void setCourses(Collection<Course> courses) {
        this.courses = courses;
    }
}
