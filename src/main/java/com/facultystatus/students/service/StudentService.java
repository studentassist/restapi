package com.facultystatus.students.service;

import com.facultystatus.core.exceptions.UniqueConstraintException;
import com.facultystatus.students.model.Student;
import com.facultystatus.core.repository.AddressRepository;
import com.facultystatus.students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private StudentRepository studentRepository;
    private AddressRepository addressRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository, AddressRepository addressRepository) {
        this.studentRepository = studentRepository;
        this.addressRepository = addressRepository;
    }

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public Optional<Student> getById(Long id) {
        return studentRepository.findById(id);
    }

    public Student createOrUpdateStudent(Student student) {
        try {
            return studentRepository.save(student);
        } catch (Exception e) {
            throw new UniqueConstraintException("Security number " + student.getSecurityNumber() + " already exists");
        }
    }

    public Student addAddress(Student student, Long addressId) {
        return addressRepository.findById(addressId)
                .map(a -> {
                    student.setAddress(a);
                    return student;
                })
                .orElse(student);
    }




}
