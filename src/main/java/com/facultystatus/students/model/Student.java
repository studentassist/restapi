package com.facultystatus.students.model;

import com.facultystatus.core.model.Person;
import com.facultystatus.courses.model.Course;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.Date;

@Entity
public class Student extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STUDENT_ID")
    private Long studentId;

    /* @OneToOne
     @PrimaryKeyJoinColumn(name="STUDENT_ID", referencedColumnName="id")
     private User user;*/

    @NotBlank(message = "Security Number is required")
    @Column(name = "SECURITY_NUMBER", unique = true)
    private String securityNumber;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "students_courses",
            joinColumns = @JoinColumn(
                    name = "student_id", referencedColumnName = "student_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "course_id", referencedColumnName = "id"))
    private Collection<Course> courses;

    @JsonFormat(pattern = "yyyy-mm-dd")
    @Column(updatable = false)
    private Date created_At;
    @JsonFormat(pattern = "yyyy-mm-dd")
    private Date updated_At;

    @PrePersist
    protected void onCreate() {
        created_At = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated_At = new Date();
    }

    public long getStudentId() {
        return studentId;
    }

   /* public User getUser() {
        return user;
    }*/

    public String getSecurityNumber() {
        return securityNumber;
    }

    public Collection<Course> getCourses() {
        return courses;
    }

    public void setCourses(Collection<Course> courses) {
        this.courses = courses;
    }
}
