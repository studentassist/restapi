package com.facultystatus.students.model;

import java.util.Collection;
import java.util.Date;

/**
 * @author Nicorici Madalina (nicorici@optymyze.com)
 */
public class StudentDTO {
    private long studentId;
    private String securityNumber;
    private String name;
    private Date dateOfBirth;
    private String gender;
    private String phoneNumber;
    private Long addressId;
    private Collection<Long> coursesIds;

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public String getSecurityNumber() {
        return securityNumber;
    }

    public void setSecurityNumber(String securityNumber) {
        this.securityNumber = securityNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Collection<Long> getCoursesIds() {
        return coursesIds;
    }

    public void setCoursesIds(Collection<Long> coursesIds) {
        this.coursesIds = coursesIds;
    }
}
