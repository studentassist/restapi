package com.facultystatus.students.endpoint;

import com.facultystatus.courses.model.CourseDTO;
import com.facultystatus.students.model.StudentDTO;
import com.facultystatus.students.model.Student;
import com.facultystatus.core.service.BasicValidationService;
import com.facultystatus.courses.service.CourseService;
import com.facultystatus.students.service.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/students")
@CrossOrigin
public class StudentController {

    private StudentService studentService;
    private BasicValidationService basicValidationService;
    private CourseService courseService;

    public StudentController(StudentService studentService, BasicValidationService basicValidationService, CourseService courseService) {
        this.studentService = studentService;
        this.basicValidationService = basicValidationService;
        this.courseService = courseService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<List<StudentDTO>> getAll() {
        List<StudentDTO> studentDTOS = studentService.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
        return new ResponseEntity<>(studentDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<StudentDTO> getById(@PathVariable Long id) {
        return studentService.getById(id)
                .map(s -> new ResponseEntity<>(convertToDto(s), HttpStatus.OK))
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/{id}/courses", method = RequestMethod.GET)
    public ResponseEntity<List<CourseDTO>> getCoursesById(@PathVariable Long id) {
        return studentService.getById(id).map(s -> s.getCourses().stream()
                .map(c -> courseService.convertToDto(c)).collect(Collectors.toList()))
                .map(courseDTOS -> new ResponseEntity<>(courseDTOS, HttpStatus.OK))
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "")
    public ResponseEntity<?> addStudent(@Valid @RequestBody StudentDTO studentDTO, BindingResult result) {
        ResponseEntity errorsMap = basicValidationService.validate(result);
        if(errorsMap != null){
            return errorsMap;
        }
        Student savedStudent = studentService.createOrUpdateStudent(convertToEntity(studentDTO));
        return new ResponseEntity<>(savedStudent, HttpStatus.CREATED);
    }

    private StudentDTO convertToDto(Student student) {
        ModelMapper modelMapper = new ModelMapper();
        StudentDTO studentDTO = modelMapper.map(student, StudentDTO.class);
        studentDTO.setName(student.getFirstName() + " " + student.getLastName());
        return studentDTO;
    }

    private Student convertToEntity(StudentDTO studentDTO) {
        ModelMapper modelMapper = new ModelMapper();
        Student student = modelMapper.map(studentDTO, Student.class);
        if (!studentDTO.getCoursesIds().isEmpty()) {
            student.setCourses(courseService.getByIds(new ArrayList<>(studentDTO.getCoursesIds())));
        }
        if(studentDTO.getAddressId()!=null){
            studentService.addAddress(student, studentDTO.getAddressId());
        }
        return student;
    }
}
