package com.facultystatus.core.repository;

import com.facultystatus.core.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nicorici Madalina (nicorici@optymyze.com)
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
