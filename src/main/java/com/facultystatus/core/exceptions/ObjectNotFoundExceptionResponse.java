package com.facultystatus.core.exceptions;

public class ObjectNotFoundExceptionResponse {

    private String message;

    public ObjectNotFoundExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
