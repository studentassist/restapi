package com.facultystatus.core.exceptions;

public class UniqueConstraintExceptionResponse {

    private String property;

    public UniqueConstraintExceptionResponse(String property){
        this.property = property;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
