package com.facultystatus.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@MappedSuperclass
public class Person {

    @NotBlank(message = "First name is required")
    @Column(name = "FIRST_NAME")
    private String firstName;

    @NotBlank(message = "Last name is required")
    @Column(name = "LAST_NAME")
    private String lastName;

    @NotNull(message = "Date of birth is required")
    @Column(name = "DATE_OF_BIRTH")
    @JsonFormat(pattern = "yyyy-mm-dd")
    private Date dateOfBirth;

    @NotBlank(message = "Gender is required")
    @Size(max = 1, message = "Please use M/F")
    private String gender;

    @NotBlank(message = "Phone number is required")
    @Column(length = 10, name = "PHONE", unique = true)
    private String phoneNumber;

    @OneToOne
    private Address address;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
