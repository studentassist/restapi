package com.facultystatus;

		import org.springframework.boot.SpringApplication;
		import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacultyStatusApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacultyStatusApplication.class, args);
	}

}
