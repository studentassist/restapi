package com.facultystatus.courses.endpoint;

import com.facultystatus.courses.model.CourseDTO;
import com.facultystatus.courses.model.GradeDTO;
import com.facultystatus.courses.model.SectionDTO;
import com.facultystatus.courses.model.Course;
import com.facultystatus.courses.model.Section;
import com.facultystatus.students.model.Student;
import com.facultystatus.core.service.BasicValidationService;
import com.facultystatus.courses.service.CourseService;
import com.facultystatus.courses.service.GradeService;
import com.facultystatus.courses.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/courses")
@CrossOrigin
public class CourseController {

    private CourseService courseService;
    private BasicValidationService basicValidationService;
    private GradeService gradeService;
    private SectionService sectionService;

    @Autowired
    public CourseController(CourseService courseService, BasicValidationService basicValidationService, GradeService gradeService, SectionService sectionService) {
        this.courseService = courseService;
        this.basicValidationService = basicValidationService;
        this.gradeService = gradeService;
        this.sectionService = sectionService;
    }

    @RequestMapping(path = "/id/{id}")
    public ResponseEntity<CourseDTO> getById(@PathVariable Long id) {
        return courseService.findById(id)
                .map(c -> new ResponseEntity<>(courseService.convertToDto(c), HttpStatus.OK))
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/{identifier}")
    public ResponseEntity<CourseDTO> getByIdentifier(@PathVariable String identifier) {
        return courseService.getByIdentifier(identifier)
                .map(c -> new ResponseEntity<>(courseService.convertToDto(c), HttpStatus.OK))
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/{id}/students")
    public ResponseEntity<List<Student>> getStudentsByCourseId(@PathVariable Long id) {
        return courseService.findById(id)
                .map(c -> new ResponseEntity<>((List<Student>) c.getStudents(), HttpStatus.OK))
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/{id}/sections")
    public ResponseEntity<List<SectionDTO>> getSectionsByCourseId(@PathVariable Long id) {
        return courseService.findById(id)
                .map(c -> new ResponseEntity<>(c.getSections().stream().map(s -> sectionService.convertToDto(s)).collect(Collectors.toList()), HttpStatus.OK))
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/{id}/sections")
    public ResponseEntity<?> addSectionToCourse(@PathVariable Long id, @Valid @RequestBody SectionDTO sectionDTO, BindingResult bindingResult) {
        Assert.isTrue(sectionDTO.getCourseId().equals(id), "Course is invalid");
        ResponseEntity errorsMap = basicValidationService.validate(bindingResult);
        if (errorsMap != null) {
            return errorsMap;
        }

        Section section = sectionService.createSection(sectionService.convertToEntity(sectionDTO), id);
        return new ResponseEntity<>(section, HttpStatus.CREATED);
    }

    @PostMapping(path = "/{id}/sections/{id}")
    public ResponseEntity<?> updateSectionToCourse(@PathVariable Long id, @Valid @RequestBody SectionDTO sectionDTO, BindingResult bindingResult) {
        Assert.isTrue(sectionDTO.getCourseId().equals(id), "Course is invalid");
        ResponseEntity errorsMap = basicValidationService.validate(bindingResult);
        if (errorsMap != null) {
            return errorsMap;
        }
        Section section = sectionService.updateSection(sectionService.convertToEntity(sectionDTO));
        return new ResponseEntity<>(section, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}/grades")
    public ResponseEntity<List<GradeDTO>> getGradesByCourseId(@PathVariable Long id) {
        return courseService.findById(id)
                .map(c -> {
                    List<GradeDTO> gradesPerSection = gradeService.getGradesPerSection(c.getSections().stream().map(Section::getId).collect(Collectors.toList()))
                            .stream().map(g -> gradeService.convertToDto(g)).collect(Collectors.toList());
                    return new ResponseEntity<>(gradesPerSection, HttpStatus.OK);
                })
                .orElse(new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "")
    public ResponseEntity<List<CourseDTO>> getAll() {
        List<CourseDTO> courses = courseService.getAll().stream().map(c -> courseService.convertToDto(c)).collect(Collectors.toList());
        return new ResponseEntity<>(courses, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<?> addCourse(@Valid @RequestBody CourseDTO courseDTO, BindingResult result) {
        ResponseEntity errorsMap = basicValidationService.validate(result);
        if (errorsMap != null) {
            return errorsMap;
        }
        Course savedCourse = courseService.createOrUpdateCourse(courseService.convertToEntity(courseDTO));
        return new ResponseEntity<>(savedCourse, HttpStatus.CREATED);
    }

    @PostMapping(path = "/{id}")
    public ResponseEntity<?> updateCourse(@PathVariable Long id, @Valid @RequestBody CourseDTO courseDTO, BindingResult result) {
        ResponseEntity errorsMap = basicValidationService.validate(result);
        if (errorsMap != null) {
            return errorsMap;
        }
        Course savedCourse = courseService.createOrUpdateCourse(courseService.convertToEntity(courseDTO));
        return new ResponseEntity<>(savedCourse, HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteCourse(@PathVariable Long id) {
        courseService.deleteCourseById(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
