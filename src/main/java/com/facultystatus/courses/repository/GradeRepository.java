package com.facultystatus.courses.repository;

import com.facultystatus.courses.model.Grade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * @author Nicorici Madalina (nicorici@optymyze.com)
 */
public interface GradeRepository extends JpaRepository<Grade, Long> {

    @Query("select g from Grade g where g.section.id in (:sectionIds)")
    List<Grade> findAllBySections(@Param("sectionIds") Collection<Long> sectionIds);
}
