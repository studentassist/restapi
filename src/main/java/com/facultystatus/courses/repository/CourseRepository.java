package com.facultystatus.courses.repository;

import com.facultystatus.courses.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    @Query("select c from Course c where c.identifier = :identifier")
    Optional<Course> findByIdentifier(@Param("identifier") String identifier);
}
