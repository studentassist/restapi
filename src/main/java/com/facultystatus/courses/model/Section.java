package com.facultystatus.courses.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Section {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Section name is required")
    private String name;

    @NotNull(message = "Section percentage is required")
    private Integer percentage;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JsonIgnore
    private Course course;

    @OneToMany(mappedBy = "section")
    private Collection<Grade> grades;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public Course getCourse() {
        return course;
    }

    public Collection<Grade> getGrades() {
        return grades;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return Objects.equals(id, section.id) &&
                Objects.equals(name, section.name) &&
                Objects.equals(percentage, section.percentage) &&
                Objects.equals(course, section.course) &&
                Objects.equals(grades, section.grades);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, percentage, course, grades);
    }
}
