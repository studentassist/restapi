package com.facultystatus.courses.model;

import com.facultystatus.students.model.Student;
import com.facultystatus.teachers.model.Teacher;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Grade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "grade_value")
    @NotNull(message = "Grade value name is required")
    @Range(min = 1, max = 10)
    private Integer gradeValue;

    @OneToOne
    private Student student;
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Section section;
    @OneToOne
    private Teacher teacher;

    @Column(updatable = false)
    private Date created_At;
    private Date updated_At;

    @PrePersist
    protected void onCreate() {
        created_At = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated_At = new Date();
    }

    public Long getId() {
        return id;
    }

    public Integer getGradeValue() {
        return gradeValue;
    }

    public Student getStudent() {
        return student;
    }

    public Section getSection() {
        return section;
    }

    public Teacher getTeacher() {
        return teacher;
    }
}
