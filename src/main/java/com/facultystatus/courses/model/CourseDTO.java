package com.facultystatus.courses.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CourseDTO {
    private Long id;
    @NotBlank(message = "Course name is required")
    private String name;
    @NotBlank(message = "Course identifier is required")
    private String identifier;
    @NotBlank(message = "Course description is required")
    private String description;
    @NotNull(message = "Number of credits is required")
    private Integer credits;
    @NotNull(message = "Course semester is required")
    private Integer semester;
    @NotNull(message = "Course year is required")
    private Integer year;
    private Long teacherId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }
}
