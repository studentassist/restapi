package com.facultystatus.courses.model;

import com.facultystatus.students.model.Student;
import com.facultystatus.teachers.model.Teacher;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Course name is required")
    @Column(unique = true)
    private String name;
    @NotBlank(message = "Course name is required")
    @Column(unique = true)
    private String identifier;
    @NotBlank(message = "Course description is required")
    private String description;
    @NotNull(message = "Number of credits is required")
    private Integer credits;
    @NotNull(message = "Course semester is required")
    @Range(min = 1, max = 6)
    private Integer semester;
    @NotNull(message = "Course year is required")
    @Range(min = 1, max = 3)
    private Integer year;

    @ManyToOne
    @JoinColumn(name = "TEACHER_ID")
    private Teacher teacher;

    //@JsonIgnore
    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    private Collection<Section> sections;

    @JsonIgnore
    @ManyToMany(mappedBy = "courses")
    private Collection<Student> students;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Collection<Section> getSections() {
        return sections;
    }

    public void setSections(Collection<Section> sections) {
        this.sections = sections;
    }

    public Collection<Student> getStudents() {
        return students;
    }

    public void setStudents(Collection<Student> students) {
        this.students = students;
    }
}
