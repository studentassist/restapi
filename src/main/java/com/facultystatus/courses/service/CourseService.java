package com.facultystatus.courses.service;

import com.facultystatus.courses.model.CourseDTO;
import com.facultystatus.core.exceptions.ObjectNotFoundException;
import com.facultystatus.core.exceptions.UniqueConstraintException;
import com.facultystatus.courses.model.Course;
import com.facultystatus.teachers.model.Teacher;
import com.facultystatus.courses.repository.CourseRepository;
import com.facultystatus.teachers.repository.TeacherRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

    private CourseRepository courseRepository;
    private TeacherRepository teacherRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository, TeacherRepository teacherRepository) {
        this.courseRepository = courseRepository;
        this.teacherRepository = teacherRepository;
    }

    public Course getById(Long id) {
        return findById(id).orElseThrow(() -> new ObjectNotFoundException("Course with id " + id + " does not exist"));
    }

    public Optional<Course> findById(Long id) {
        return courseRepository.findById(id);
    }

    public Optional<Course> getByIdentifier(String identifier) {
        return courseRepository.findByIdentifier(identifier);
    }

    public List<Course> getByIds(List<Long> ids) {
        return courseRepository.findAllById(ids);
    }

    public List<Course> getAll() {
        return courseRepository.findAll();
    }

    public Course createOrUpdateCourse(Course course) {
        try {
            return courseRepository.save(course);
        } catch (Exception e) {
            throw new UniqueConstraintException("Course name " + course.getName() + " already exists");
        }
    }

    public void deleteCourseById(Long courseId) {
        Optional<Course> courseOpt = courseRepository.findById(courseId);
        if (courseOpt.isPresent()) {
            courseRepository.deleteById(courseId);
        } else {
            throw new ObjectNotFoundException("Course to be deleted does not exists");
        }
    }

    public CourseDTO convertToDto(Course course) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(course, CourseDTO.class);
    }

    public Course convertToEntity(CourseDTO courseDTO) {
        ModelMapper modelMapper = new ModelMapper();
        Course course = modelMapper.map(courseDTO, Course.class);
        if (courseDTO.getTeacherId() != null) {
            Optional<Teacher> teacherOptional = teacherRepository.findById(courseDTO.getTeacherId());
            teacherOptional.ifPresent(course::setTeacher);
        }
        return course;
    }
}
