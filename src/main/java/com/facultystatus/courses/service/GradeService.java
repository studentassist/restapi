package com.facultystatus.courses.service;

import com.facultystatus.courses.model.GradeDTO;
import com.facultystatus.courses.model.Grade;
import com.facultystatus.courses.repository.GradeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nicorici Madalina (nicorici@optymyze.com)
 */
@Service
public class GradeService {

    private GradeRepository gradeRepository;

    @Autowired
    public GradeService(GradeRepository gradeRepository) {
        this.gradeRepository = gradeRepository;
    }

    public List<Grade> getGradesPerSection(List<Long> sectionIds) {
        return gradeRepository.findAllBySections(sectionIds);
    }

    public GradeDTO convertToDto(Grade grade) {
        ModelMapper modelMapper = new ModelMapper();
        GradeDTO gradeDTO = modelMapper.map(grade, GradeDTO.class);
        if (grade.getStudent() != null) {
            gradeDTO.setStudentId(grade.getStudent().getStudentId());
        }
        return gradeDTO;
    }

    public Grade convertToEntity(GradeDTO gradeDTO) {
        ModelMapper modelMapper = new ModelMapper();
        Grade grade = modelMapper.map(gradeDTO, Grade.class);
        return grade;
    }
}
