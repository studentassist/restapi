package com.facultystatus.courses.service;

import com.facultystatus.courses.model.SectionDTO;
import com.facultystatus.core.exceptions.ObjectNotFoundException;
import com.facultystatus.courses.model.Course;
import com.facultystatus.courses.model.Section;
import com.facultystatus.courses.repository.CourseRepository;
import com.facultystatus.courses.repository.SectionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SectionService {

    private final SectionRepository sectionRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public SectionService(SectionRepository sectionRepository, CourseRepository courseRepository) {
        this.sectionRepository = sectionRepository;
        this.courseRepository = courseRepository;
    }

    public SectionDTO convertToDto(Section section) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(section, SectionDTO.class);
    }

    public Section convertToEntity(SectionDTO sectionDTO) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(sectionDTO, Section.class);
    }

    public Section createSection(Section section, Long courseId) {
        Optional<Course> course = courseRepository.findById(courseId);
        if (!course.isPresent()) {
            throw new ObjectNotFoundException("Course with id " + courseId + " does not exist!");
        }
        section.setCourse(course.get());
        return sectionRepository.save(section);

    }

    public Section updateSection(Section section) {
        Course sectionCourse = section.getCourse();
        if (sectionCourse == null) {
            throw new IllegalArgumentException("Course cannot be null");
        }
        Optional<Course> course = courseRepository.findById(sectionCourse.getId());
        if (!course.isPresent()) {
            throw new ObjectNotFoundException("Course with id " + sectionCourse.getId() + " does not exist!");
        }
        return sectionRepository.save(section);

    }

}
