package com.facultystatus.services;

import com.facultystatus.users.model.User;
import com.facultystatus.users.service.UserService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    @Ignore
    public void whenAppStarts_userExists() {
        List<User> users = userService.getAll();
        assertEquals(3, users.size());
    }

}