--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

-- Started on 2020-04-12 23:07:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2944 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 206 (class 1259 OID 23927)
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.address (
    id bigint NOT NULL,
    city character varying(255),
    country character varying(255),
    street character varying(255),
    zip_code bigint,
    CONSTRAINT address_zip_code_check CHECK (((zip_code <= 999999) AND (zip_code >= 100000)))
);


ALTER TABLE public.address OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 23925)
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_id_seq OWNER TO postgres;

--
-- TOC entry 2945 (class 0 OID 0)
-- Dependencies: 205
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;


--
-- TOC entry 208 (class 1259 OID 23939)
-- Name: course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.course (
    id bigint NOT NULL,
    credits integer NOT NULL,
    description character varying(255),
    identifier character varying(255),
    name character varying(255),
    semester integer NOT NULL,
    year integer NOT NULL,
    teacher_id bigint,
    CONSTRAINT course_semester_check CHECK (((semester <= 6) AND (semester >= 1))),
    CONSTRAINT course_year_check CHECK (((year >= 1) AND (year <= 3)))
);


ALTER TABLE public.course OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 23937)
-- Name: course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_id_seq OWNER TO postgres;

--
-- TOC entry 2946 (class 0 OID 0)
-- Dependencies: 207
-- Name: course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.course_id_seq OWNED BY public.course.id;


--
-- TOC entry 210 (class 1259 OID 23952)
-- Name: grade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grade (
    id bigint NOT NULL,
    created_at timestamp without time zone,
    grade_value integer NOT NULL,
    updated_at timestamp without time zone,
    section_id bigint,
    student_student_id bigint,
    teacher_teacher_id bigint,
    CONSTRAINT grade_grade_value_check CHECK (((grade_value <= 10) AND (grade_value >= 1)))
);


ALTER TABLE public.grade OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 23950)
-- Name: grade_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grade_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grade_id_seq OWNER TO postgres;

--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 209
-- Name: grade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grade_id_seq OWNED BY public.grade.id;


--
-- TOC entry 204 (class 1259 OID 18296)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 23961)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 23959)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 211
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- TOC entry 214 (class 1259 OID 23969)
-- Name: section; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.section (
    id bigint NOT NULL,
    name character varying(255),
    percentage integer NOT NULL,
    course_id bigint
);


ALTER TABLE public.section OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 23967)
-- Name: section_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.section_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.section_id_seq OWNER TO postgres;

--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 213
-- Name: section_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.section_id_seq OWNED BY public.section.id;


--
-- TOC entry 216 (class 1259 OID 23977)
-- Name: student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.student (
    student_id bigint NOT NULL,
    date_of_birth timestamp without time zone NOT NULL,
    first_name character varying(255),
    gender character varying(1),
    last_name character varying(255),
    phone character varying(10),
    created_at timestamp without time zone,
    security_number character varying(255),
    updated_at timestamp without time zone,
    address_id bigint
);


ALTER TABLE public.student OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 23975)
-- Name: student_student_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.student_student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_student_id_seq OWNER TO postgres;

--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 215
-- Name: student_student_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.student_student_id_seq OWNED BY public.student.student_id;


--
-- TOC entry 217 (class 1259 OID 23986)
-- Name: students_courses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.students_courses (
    student_id bigint NOT NULL,
    course_id bigint NOT NULL
);


ALTER TABLE public.students_courses OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 23991)
-- Name: teacher; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teacher (
    teacher_id bigint NOT NULL,
    date_of_birth timestamp without time zone NOT NULL,
    first_name character varying(255),
    gender character varying(1),
    last_name character varying(255),
    phone character varying(10),
    hire_date timestamp without time zone NOT NULL,
    title character varying(255),
    address_id bigint
);


ALTER TABLE public.teacher OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 23989)
-- Name: teacher_teacher_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.teacher_teacher_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teacher_teacher_id_seq OWNER TO postgres;

--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 218
-- Name: teacher_teacher_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.teacher_teacher_id_seq OWNED BY public.teacher.teacher_id;


--
-- TOC entry 221 (class 1259 OID 24002)
-- Name: user_table; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_table (
    id bigint NOT NULL,
    created_at timestamp without time zone,
    email character varying(255),
    enabled boolean NOT NULL,
    password character varying(255),
    updated_at timestamp without time zone,
    username character varying(255)
);


ALTER TABLE public.user_table OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 24000)
-- Name: user_table_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_table_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_table_id_seq OWNER TO postgres;

--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 220
-- Name: user_table_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_table_id_seq OWNED BY public.user_table.id;


--
-- TOC entry 222 (class 1259 OID 24011)
-- Name: users_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_roles (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.users_roles OWNER TO postgres;

--
-- TOC entry 2765 (class 2604 OID 23930)
-- Name: address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);


--
-- TOC entry 2767 (class 2604 OID 23942)
-- Name: course id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course ALTER COLUMN id SET DEFAULT nextval('public.course_id_seq'::regclass);


--
-- TOC entry 2770 (class 2604 OID 23955)
-- Name: grade id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grade ALTER COLUMN id SET DEFAULT nextval('public.grade_id_seq'::regclass);


--
-- TOC entry 2772 (class 2604 OID 23964)
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- TOC entry 2773 (class 2604 OID 23972)
-- Name: section id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.section ALTER COLUMN id SET DEFAULT nextval('public.section_id_seq'::regclass);


--
-- TOC entry 2774 (class 2604 OID 23980)
-- Name: student student_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student ALTER COLUMN student_id SET DEFAULT nextval('public.student_student_id_seq'::regclass);


--
-- TOC entry 2775 (class 2604 OID 23994)
-- Name: teacher teacher_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher ALTER COLUMN teacher_id SET DEFAULT nextval('public.teacher_teacher_id_seq'::regclass);


--
-- TOC entry 2776 (class 2604 OID 24005)
-- Name: user_table id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_table ALTER COLUMN id SET DEFAULT nextval('public.user_table_id_seq'::regclass);


--
-- TOC entry 2778 (class 2606 OID 23936)
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- TOC entry 2780 (class 2606 OID 23949)
-- Name: course course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT course_pkey PRIMARY KEY (id);


--
-- TOC entry 2786 (class 2606 OID 23958)
-- Name: grade grade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grade
    ADD CONSTRAINT grade_pkey PRIMARY KEY (id);


--
-- TOC entry 2788 (class 2606 OID 23966)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 2790 (class 2606 OID 23974)
-- Name: section section_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.section
    ADD CONSTRAINT section_pkey PRIMARY KEY (id);


--
-- TOC entry 2792 (class 2606 OID 23985)
-- Name: student student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_pkey PRIMARY KEY (student_id);


--
-- TOC entry 2798 (class 2606 OID 23999)
-- Name: teacher teacher_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher
    ADD CONSTRAINT teacher_pkey PRIMARY KEY (teacher_id);


--
-- TOC entry 2800 (class 2606 OID 24023)
-- Name: teacher uk_243jo4296giohklco1097tf1y; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher
    ADD CONSTRAINT uk_243jo4296giohklco1097tf1y UNIQUE (phone);


--
-- TOC entry 2782 (class 2606 OID 24017)
-- Name: course uk_4xqvdpkafb91tt3hsb67ga3fj; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT uk_4xqvdpkafb91tt3hsb67ga3fj UNIQUE (name);


--
-- TOC entry 2794 (class 2606 OID 24019)
-- Name: student uk_5s5e6lj1siq6ef1tm7p2g4uul; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT uk_5s5e6lj1siq6ef1tm7p2g4uul UNIQUE (phone);


--
-- TOC entry 2796 (class 2606 OID 24021)
-- Name: student uk_dyg7fjeb8gcb71i5cemvgct6k; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT uk_dyg7fjeb8gcb71i5cemvgct6k UNIQUE (security_number);


--
-- TOC entry 2802 (class 2606 OID 24025)
-- Name: user_table uk_eamk4l51hm6yqb8xw37i23kb5; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_table
    ADD CONSTRAINT uk_eamk4l51hm6yqb8xw37i23kb5 UNIQUE (email);


--
-- TOC entry 2804 (class 2606 OID 24027)
-- Name: user_table uk_en3wad7p8qfu8pcmh62gvef6v; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_table
    ADD CONSTRAINT uk_en3wad7p8qfu8pcmh62gvef6v UNIQUE (username);


--
-- TOC entry 2784 (class 2606 OID 24015)
-- Name: course uk_qmo2lswtcesx5vyn99bbgdfa4; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT uk_qmo2lswtcesx5vyn99bbgdfa4 UNIQUE (identifier);


--
-- TOC entry 2806 (class 2606 OID 24010)
-- Name: user_table user_table_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_table
    ADD CONSTRAINT user_table_pkey PRIMARY KEY (id);


--
-- TOC entry 2815 (class 2606 OID 24068)
-- Name: teacher fk4lys47e3qdr8qqen6k19sj280; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher
    ADD CONSTRAINT fk4lys47e3qdr8qqen6k19sj280 FOREIGN KEY (address_id) REFERENCES public.address(id);


--
-- TOC entry 2809 (class 2606 OID 24038)
-- Name: grade fkagm3qsmjrdpsgd0d57ad205sm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grade
    ADD CONSTRAINT fkagm3qsmjrdpsgd0d57ad205sm FOREIGN KEY (student_student_id) REFERENCES public.student(student_id);


--
-- TOC entry 2812 (class 2606 OID 24053)
-- Name: student fkcaf6ht0hfw93lwc13ny0sdmvo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT fkcaf6ht0hfw93lwc13ny0sdmvo FOREIGN KEY (address_id) REFERENCES public.address(id);


--
-- TOC entry 2810 (class 2606 OID 24043)
-- Name: grade fkckbn9dralj3udo44ak1m6c6vg; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grade
    ADD CONSTRAINT fkckbn9dralj3udo44ak1m6c6vg FOREIGN KEY (teacher_teacher_id) REFERENCES public.teacher(teacher_id);


--
-- TOC entry 2813 (class 2606 OID 24058)
-- Name: students_courses fkd6vd2y2gdvqap78cu28i6xki5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students_courses
    ADD CONSTRAINT fkd6vd2y2gdvqap78cu28i6xki5 FOREIGN KEY (course_id) REFERENCES public.course(id);


--
-- TOC entry 2814 (class 2606 OID 24063)
-- Name: students_courses fkfbiw8vd6a6fxgjlqi99c977al; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students_courses
    ADD CONSTRAINT fkfbiw8vd6a6fxgjlqi99c977al FOREIGN KEY (student_id) REFERENCES public.student(student_id);


--
-- TOC entry 2808 (class 2606 OID 24033)
-- Name: grade fkkfjq621x0dlrte8ri00shrrei; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grade
    ADD CONSTRAINT fkkfjq621x0dlrte8ri00shrrei FOREIGN KEY (section_id) REFERENCES public.section(id);


--
-- TOC entry 2817 (class 2606 OID 24078)
-- Name: users_roles fknc1kw0n8q07u1gf44x0vghw8r; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT fknc1kw0n8q07u1gf44x0vghw8r FOREIGN KEY (user_id) REFERENCES public.user_table(id);


--
-- TOC entry 2811 (class 2606 OID 24048)
-- Name: section fkoy8uc0ftpivwopwf5ptwdtar0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.section
    ADD CONSTRAINT fkoy8uc0ftpivwopwf5ptwdtar0 FOREIGN KEY (course_id) REFERENCES public.course(id);


--
-- TOC entry 2807 (class 2606 OID 24028)
-- Name: course fksybhlxoejr4j3teomm5u2bx1n; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT fksybhlxoejr4j3teomm5u2bx1n FOREIGN KEY (teacher_id) REFERENCES public.teacher(teacher_id);


--
-- TOC entry 2816 (class 2606 OID 24073)
-- Name: users_roles fkt4v0rrweyk393bdgt107vdx0x; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT fkt4v0rrweyk393bdgt107vdx0x FOREIGN KEY (role_id) REFERENCES public.role(id);


-- Completed on 2020-04-12 23:07:37

--
-- PostgreSQL database dump complete
--

